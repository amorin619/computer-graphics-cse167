#include <stdlib.h>
#include <cmath>
#include "Cube.h"
#include "Matrix4.h"
#include "Window.h"

using namespace std;

Cube::Cube() {
  angle = 0.0;
}

Matrix4& Cube::getMatrix() {
    return model2world;
}

void Cube::setMatrix(Matrix4& m) {
	model2world = m;
}


void Cube::spin(double deg)   // deg is in degrees
{
	Matrix4 m;
	m.makeRotateY(deg);
	model2world = model2world * m;
}

void Cube::scale(double amt) {
	Matrix4 m;
	m.makeScale(amt, amt, amt);
	model2world = model2world * m;
}

void Cube::translate(double x, double y, double z) {
	Matrix4 m;
	m.makeTranslate(x, y, z);
	model2world = m * model2world;
}

void Cube::orbitZ(double deg) {
	Matrix4 r;
	r.makeRotateZ(deg);
	model2world = r * model2world;
}

// TODO: orientation, size, and color
void Cube::reset() {
	Matrix4 m;
	m.identity();
	Window::color[0] = 0;
	Window::color[1] = 1;
	Window::color[2] = 0;
	model2world = m;
}

void Cube::applyVelocity() {
	Matrix4 m;
	if ((model2world.m[0][3] > SIDE_THRESHOLD && velocity.x > 0) || (model2world.m[0][3] < -SIDE_THRESHOLD && velocity.x < 0)) {
		velocity.set(-velocity.x * .8, velocity.y, 0);
	}
	if ((model2world.m[1][3] > THRESHOLD && velocity.y > 0) || (model2world.m[1][3] < -THRESHOLD && velocity.y < 0)) {
		velocity.set(velocity.x, -velocity.y * .8, 0);
	}
	m.makeTranslate(velocity.x, velocity.y, 0);
	velocity.y = velocity.y - ACCELERATION;
	model2world = m * model2world;
}

void Cube::randomizeVelocity() {
	velocity.set((rand() % (int)RAND_AMT - (RAND_AMT / 2)) / RAND_AMT, -.25, 0);
}

void Cube::print() {
	model2world.printPosition();
}
