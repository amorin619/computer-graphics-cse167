#include "XYZParser.h"
#include <stdio.h>
#include <GL/glut.h>
#include <iostream>

XYZParser::XYZParser()
{
	resetBounds();
}


XYZParser::~XYZParser()
{
	
}

//stores points into XYZVector.
void XYZParser::parseFile(string fileName)
{
	resetBounds();
	XYZVector.clear();
	FILE * pFile;
	pFile = fopen(fileName.c_str(), "r");

	while (fscanf(pFile, "%lf %lf %lf %lf %lf %lf\n", &XYZLine.x, &XYZLine.y, &XYZLine.z, &XYZLine.xNorm, &XYZLine.yNorm, &XYZLine.zNorm) != EOF)
	{
		normalizeStruct();
		XYZVector.push_back(XYZLine);
		recalculateBounds();
	}
	fclose(pFile);
	cout << "finished parsing." << endl;
}

void XYZParser::displayParsedFile()
{
	//glPointSize(8);
	double length = maxZ - minZ; //absolute difference of farthest point from closest point.
	double maxPointRadius = .001; //largest size of quad.
	double x, y, z, newRadius, shrinkage, halfLength, doubleLength;
	halfLength = length / 2;
	doubleLength = length * 2;
	/*glBegin(GL_POINTS);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_POINT_SMOOTH);
		glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
=======
	glBegin(GL_POINTS);
	glColor3f(1, 1, 1); //white
>>>>>>> a861cb9f48d99c30981dea79ee2a178f08d02c9c
		for (unsigned int i = 0; i < XYZVector.size(); i++)
		{
			glColor3f(0.5*(XYZVector[i].xNorm + 1), 0.5*(XYZVector[i].yNorm + 1), 0.5*(XYZVector[i].zNorm + 1));
			glNormal3d(XYZVector[i].xNorm, XYZVector[i].yNorm, XYZVector[i].zNorm);
			glVertex3d(XYZVector[i].x, XYZVector[i].y, XYZVector[i].z);
		}
	glEnd();
	*/
	glBegin(GL_QUADS);
	for (unsigned int i = 0; i < XYZVector.size(); i++)
	{
		x = XYZVector[i].x;
		y = XYZVector[i].y; 
		z = XYZVector[i].z;
		shrinkage = (z - minZ) / (length * 2) + halfLength; //scale all points between full size and half of full size.(result will be somewhere between .5 and 1.
		newRadius = maxPointRadius*shrinkage;
		glColor3f(0.5*(XYZVector[i].xNorm + 1), 0.5*(XYZVector[i].yNorm + 1), 0.5*(XYZVector[i].zNorm + 1));
		glNormal3d(0, 0, 1); //face camera
		glVertex3d(XYZVector[i].x - newRadius, XYZVector[i].y + newRadius, XYZVector[i].z);
		glVertex3d(XYZVector[i].x + newRadius, XYZVector[i].y + newRadius, XYZVector[i].z);
		glVertex3d(XYZVector[i].x + newRadius, XYZVector[i].y - newRadius, XYZVector[i].z);
		glVertex3d(XYZVector[i].x - newRadius, XYZVector[i].y - newRadius, XYZVector[i].z);
	}
	glEnd();
}

void XYZParser::resetBounds()
{
	minX = 100000;
	minY = 100000;
	minZ = 100000;
	maxX = -100000;
	maxY = -100000;
	maxZ = -100000;
}

void XYZParser::recalculateBounds()
{
	if (XYZLine.x > maxX){ maxX = XYZLine.x; }
	if (XYZLine.y > maxY){ maxY = XYZLine.y; }
	if (XYZLine.z > maxZ){ maxZ = XYZLine.z; }
	if (XYZLine.x < minX){ minX = XYZLine.x; }
	if (XYZLine.y < minY){ minY = XYZLine.y; }
	if (XYZLine.z < minZ){ minZ = XYZLine.z; }
}

void XYZParser::normalizeStruct()
{
	Vector3 norms(XYZLine.xNorm, XYZLine.yNorm, XYZLine.zNorm);
	norms.normalize();
	XYZLine.xNorm = norms.getX();
	XYZLine.yNorm = norms.getY();
	XYZLine.zNorm = norms.getZ();
}
Vector3 XYZParser::getCenter()
{
	double midX = maxX - (maxX - minX) / 2;
	double midY = maxY - (maxY - minY) / 2;
	double midZ = maxZ - (maxZ - minZ) / 2;
	Vector3 retVec(midX, midY, midZ);
	return retVec;
}

void XYZParser::printBounds()
{
	cout << "minX=" << minX << ", ";
	cout << "minY=" << minY << ", ";
	cout << "minZ=" << minZ << ", " << endl;
	cout << "maxX=" << maxX << ", ";
	cout << "maxY=" << maxY << ", ";
	cout << "maxZ=" << maxZ << ", " << endl;
	Vector3 center = getCenter();
	cout << "centerX=" << center.getX() << ", ";;
	cout << "centerY=" << center.getY() << ", ";;
	cout << "centerZ=" << center.getZ() << endl;
}