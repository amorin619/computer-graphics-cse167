#include "Vector3.h"
#include <assert.h>
Vector3::Vector3(double x, double y, double z) {
	this->x = x;
	this->y = y;
	this->z = z;
}

//default constructor
Vector3::Vector3() {
	x = 0;
	y = 0;
	z = 0;
}


Vector3 Vector3::operator+(const Vector3& v2) {
	Vector3 retVector;
	retVector.x = x + v2.x;
	retVector.y = y + v2.y;
	retVector.z = z + v2.z;
	return retVector;
}

Vector3 Vector3::operator-(const Vector3& v2) {
	Vector3 retVector = Vector3(0, 0, 0);
	retVector.x = x - v2.x;
	retVector.y = y - v2.y;
	retVector.z = z - v2.z;
	return retVector;
}

Vector3& Vector3::operator=(const Vector3& v2) {
	if (this != &v2) {
		x = v2.x;
		y = v2.y;
		z = v2.z;
	}
	return *this;
}

Vector3& Vector3::operator/(const double divisor)
{
	if (divisor != 0)
	{
		x = x / divisor;
		y = y / divisor;
		z = z / divisor;
	}
	return *this;
}

void Vector3::negate() {
	x = -1 * x;
	y = -1 * y;
	z = -1 * z;
}

void Vector3::scale(double s) {
	x = s * x;
	y = s * y;
	z = s * z;
}

void Vector3::dot(const Vector3& v1, const Vector3& v2) {
	x = v1.x * v2.x;
	y = v1.y * v2.y;
	z = v1.z * v2.z;
}

void Vector3::cross(const Vector3& v1, const Vector3& v2) {
	x = v1.y * v2.z - v1.z * v2.y;
	y = v1.z * v2.x - v1.x * v2.z;
	z = v1.x * v2.y - v1.y * v2.x;
}

double Vector3::length() {
	return sqrt(x*x + y*y + z*z);
}

void Vector3::normalize() {
	double len = length();
	x = x / len;
	y = y / len;
	z = z / len;
}

void Vector3::set(double x, double y, double z) {
	this->x = x;
	this->y = y;
	this->z = z;
}


void Vector3::print(string comment) {
	cout << "x = " << x << ", ";
	cout << "y = " << y << ", ";
	cout << "z = " << z << "." << endl;
}

double Vector3::getIndex(int index)
{
	switch (index)
	{
		case 0:
			return x;
			break;
		case 1:
			return y;
			break;
		case 2:
			return z;
			break;
		default:
			assert(false); //requested invalid index.
	}
}
Vector3::~Vector3() {

}