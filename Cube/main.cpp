#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include <GL/glut.h>

#include "main.h"
#include "Window.h"
#include "Cube.h"
#include "Camera.h"
#include "Matrix4.h"

using namespace std;

namespace Globals
{
  Cube cube;
  Camera camera;
  XYZParser xyzParser;
};

const int DELTA_TRANSLATE = 5;

void processSpecialKeys(int key, int x, int y) {

	switch (key) {
	case GLUT_KEY_F1:
		//image 1:
		glDisable(GL_LIGHTING);

		Globals::camera.setE(0, 10, 10);
		Globals::camera.setD(0, 0, 0);
		Globals::camera.setUp(0, 1, 0);

		Window::houseIsActive = true;
		Window::sphereIsActive = false;
		Window::cubeIsActive = false;
		Window::bunnyIsActive = false;
		Window::dragonIsActive = false;
		break;

	case GLUT_KEY_F2:
		//image 2:
		glDisable(GL_LIGHTING);

		Globals::camera.setE(-15, 5, 10);
		Globals::camera.setD(-5, 0, 0);
		Globals::camera.setUp(0, 1, 0.5);

		Window::houseIsActive = true;
		Window::sphereIsActive = false;
		Window::cubeIsActive = false;
		Window::bunnyIsActive = false;
		Window::dragonIsActive = false;
		break;

	case GLUT_KEY_F3:
		glEnable(GL_LIGHTING);
		Window::houseIsActive = false;
		Window::sphereIsActive = false;
		Window::cubeIsActive = true;
		Window::bunnyIsActive = false;
		Window::dragonIsActive = false;
		break;

	case GLUT_KEY_F4:
		glEnable(GL_LIGHTING);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_POINT_SMOOTH);
		glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
		Window::houseIsActive = false;
		Window::sphereIsActive = false;
		Window::cubeIsActive = false;
		Window::bunnyIsActive = true;
		Window::dragonIsActive = false;
		Globals::xyzParser.parseFile("bunny.xyz");
		Globals::xyzParser.printBounds();
		Window::canPrintBunny = true;
		break;

	case GLUT_KEY_F5:
		glEnable(GL_LIGHTING);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_POINT_SMOOTH);
		glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
		Window::houseIsActive = false;
		Window::sphereIsActive = false;
		Window::cubeIsActive = false;
		Window::bunnyIsActive = false;
		Window::dragonIsActive = true;
		Globals::xyzParser.parseFile("dragon.xyz");
		Globals::xyzParser.printBounds();
		Window::canPrintDragon = true;
		break;
	default:
		Window::canPrintDragon = false;
		Window::canPrintBunny = false;
		break;
	}

}

void processNormalKeys(unsigned char key, int x, int y) {

	switch (key) {
	case 't':
		//toggle spin clockwise/counterclockwise.
		Window::rotationDirection = -Window::rotationDirection;
		break;
	case 'x':
		//translate cube left
		Globals::cube.translate(-DELTA_TRANSLATE, 0, 0);
		break;
	case 'X':
		Globals::cube.translate(DELTA_TRANSLATE, 0, 0);
		break;
	case 'y':
		Globals::cube.translate(0, -DELTA_TRANSLATE, 0);
		break;
	case 'Y':
		Globals::cube.translate(0, DELTA_TRANSLATE, 0);
		break;
	case 'z':
		Globals::cube.translate(0, 0, DELTA_TRANSLATE);
		break;
	case 'Z':
		Globals::cube.translate(0, 0, -DELTA_TRANSLATE);
		break;
	case 'r':
		//reset cube position, orientation, size, and color.
		Globals::cube.reset();
		break;
	case 'o':
		//orbit cube clockwise 10 degrees per press about the openGL window's z axis.
		Globals::cube.orbitZ(10);
		break;
	case 'O':
		//orbit cube counter-clockwise 10 degrees per press.
		Globals::cube.orbitZ(-10);
		break;
	case 's':
		//scale cube down.
		Globals::cube.scale(.5);
		break;
	case 'S':
		//scale cube up.
		Globals::cube.scale(2);
		break;
	case 'b':
		Globals::cube.reset();
		Globals::cube.randomizeVelocity();
		Window::sphereIsActive = !Window::sphereIsActive;
		Window::cubeIsActive = !Window::cubeIsActive;
		break;
	default:
		break;
	}
	Globals::cube.print();
}

int main(int argc, char *argv[])
{	
  float specular[]  = {1.0, 1.0, 1.0, 1.0};
  float shininess[] = {100.0};
  float position[]  = {0.0, 10.0, 1.0, 0.0};	// lightsource position
  
  glutInit(&argc, argv);      	      	      // initialize GLUT
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);   // open an OpenGL context with double buffering, RGB colors, and depth buffering
  glutInitWindowSize(Window::width, Window::height);      // set initial window size
  glutCreateWindow("OpenGL Cube");    	      // open window and set window title

  glEnable(GL_DEPTH_TEST);            	      // enable depth buffering
  glClear(GL_DEPTH_BUFFER_BIT);       	      // clear depth buffer
  glClearColor(0.0, 0.0, 0.0, 0.0);   	      // set clear color to black
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);  // set polygon drawing mode to fill front and back of each polygon
  glDisable(GL_CULL_FACE);     // disable backface culling to render both sides of polygons
  glShadeModel(GL_SMOOTH);             	      // set shading to smooth
  glMatrixMode(GL_PROJECTION); 
  
  // Generate material properties:
  glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular);
  glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, shininess);
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
  glEnable(GL_COLOR_MATERIAL);
  
  // Generate light source:
  glLightfv(GL_LIGHT0, GL_POSITION, position);
  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
  
  // Install callback functions:
  glutDisplayFunc(Window::displayCallback);
  glutReshapeFunc(Window::reshapeCallback);
  glutIdleFunc(Window::idleCallback);
    
  // Initialize cube matrix:
  Globals::cube.getMatrix().identity();
  
  //process the keys:
  glutKeyboardFunc(processNormalKeys);
  glutSpecialFunc(processSpecialKeys);

  glutMainLoop();
  return 0;
}

bool sphereAtBottom() {
	//if (yPos - velocity < 0)
		return true;
	return false;
}

void moveSphere() {
	int acceleration = 8;
	double velocity = velocity + acceleration;
	if (sphereAtBottom()) {
		velocity = -velocity;
	}
}