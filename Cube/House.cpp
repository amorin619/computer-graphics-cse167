#include "House.h"
#include <iostream>
#include <GL/glut.h>
using namespace std;

float House::vertices[nVerts * 3] = {
	-4, -4, 4, 4, -4, 4, 4, 4, 4, -4, 4, 4,     // front face
	-4, -4, -4, -4, -4, 4, -4, 4, 4, -4, 4, -4, // left face
	4, -4, -4, -4, -4, -4, -4, 4, -4, 4, 4, -4,  // back face
	4, -4, 4, 4, -4, -4, 4, 4, -4, 4, 4, 4,     // right face
	4, 4, 4, 4, 4, -4, -4, 4, -4, -4, 4, 4,     // top face
	-4, -4, 4, -4, -4, -4, 4, -4, -4, 4, -4, 4, // bottom face

	-20, -4, 20, 20, -4, 20, 20, -4, -20, -20, -4, -20, // grass
	4, 4, 4, 4, 4, -4, 0, 8, -4, 0, 8, 4,               // left slope
	-4, 4, 4, 0, 8, 4, 0, 8, -4, -4, 4, -4,             // right slope
	4, 4, -4, -4, 4, -4, 0, 8, -4,                  // rear attic wall
	-4, 4, 4, 4, 4, 4, 0, 8, 4 };                       // front attic wall

// These are the RGB colors corresponding to the vertices, in the same order
float House::colors[nVerts*3] = {
	1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0,  // front is red
	0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0,  // left is green
	1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0,  // back is red
	0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0,  // right is green
	0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1,  // top is blue
	0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1,  // bottom is blue

	0, 0.5, 0, 0, 0.5, 0, 0, 0.5, 0, 0, 0.5, 0, // grass is dark green
	1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0,         // left slope is green
	0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0,         // right slope is red
	0, 0, 1, 0, 0, 1, 0, 0, 1,              // rear attic wall is red
	0, 0, 1, 0, 0, 1, 0, 0, 1};                // front attic wall is blue

// The index data stores the connectivity of the triangles; 
// index 0 refers to the first triangle defined above
int House::indices[60] = {
	0, 2, 3, 0, 1, 2,      // front face
	4, 6, 7, 4, 5, 6,      // left face
	8, 10, 11, 8, 9, 10,     // back face
	12, 14, 15, 12, 13, 14,   // right face
	16, 18, 19, 16, 17, 18,   // top face
	20, 22, 23, 20, 21, 22,   // bottom face

	24, 26, 27, 24, 25, 26,   // grass
	31, 33, 34, 31, 32, 33,   // left slope
	35, 37, 38, 35, 36, 37,   // right slope
	39, 40, 41,            // rear attic wall
	28, 29, 30};             // front attic wall

House::House()
{

}


House::~House()
{

}

void House::makeHouse()
{
	glMatrixMode(GL_MODELVIEW);
	int squareEnd = nVerts * 3 - 3 * 6;
	//squares:
	glBegin(GL_QUADS);
	for (int i = 0; i < squareEnd; i += 3)
	{
		glColor3f(colors[i], colors[i + 1], colors[i + 2]);		// This makes the cube green; the parameters are for red, green and blue. 
		// To change the color of the other faces you will need to repeat this call before each face is drawn.
		// Draw front face:
		//glNormal3f(0.0, 0.0, 1.0);
		glVertex3f(vertices[i], vertices[i + 1], vertices[i + 2]);
	}
	glEnd();

	//attics:
	glBegin(GL_TRIANGLES);
	for (int i = squareEnd; i < nVerts * 3; i += 3)
	{
		glColor3f(colors[i], colors[i + 1], colors[i + 2]);		// This makes the cube green; the parameters are for red, green and blue. 
		// To change the color of the other faces you will need to repeat this call before each face is drawn.
		// Draw front face:
		//glNormal3f(0.0, 0.0, 1.0);
		glVertex3f(vertices[i], vertices[i + 1], vertices[i + 2]);
	}
	glEnd();
}