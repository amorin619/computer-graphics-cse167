#ifndef _MAIN_H_
#define _MAIN_H_

#include "Cube.h"
#include "Camera.h"
#include "XYZParser.h"
namespace Globals
{
	extern Cube cube;
	extern Camera camera;
	extern XYZParser xyzParser;
};

#endif