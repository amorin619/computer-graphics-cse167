#ifndef _HOUSE_
#define _HOUSE_
class House
{
public:
	House();
	~House();
	void makeHouse();
private:
	// This data structure defines a simple house

	static const int nVerts = 42;    // your vertex array needs to have this many entries

	// These are the x,y,z coordinates of the vertices of the triangles
	static float vertices[nVerts * 3];

	// These are the RGB colors corresponding to the vertices, in the same order
	static float colors[nVerts * 3];

	// The index data stores the connectivity of the triangles; 
	// index 0 refers to the first triangle defined above
	static int indices[60];
};
#endif
