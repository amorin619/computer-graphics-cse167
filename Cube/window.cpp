#include <iostream>

#include <GL/glut.h>
#include "Window.h"
#include "Cube.h"
#include "Matrix4.h"
#include "Camera.h"
#include "main.h"
#include "XYZParser.h"

using namespace std;
House house;
int Window::width = 512;   // set window width in pixels here
int Window::height = 512;   // set window height in pixels here
bool Window::sphereIsActive = false;
bool Window::cubeIsActive = true;
bool Window::houseIsActive = false;
bool Window::bunnyIsActive = false;
bool Window::dragonIsActive = false;
double Window::rotationDirection = 1.0;
double Window::color[3] = {0, 1.0, 0 };
bool Window::canPrintBunny = false;
bool Window::canPrintDragon = false;
/*
double Window::xPos = 0;
double Window::yPos = 0;
double Window::zPos = 0;

*/


//----------------------------------------------------------------------------
// Callback method called when system is idle.
void Window::idleCallback()
{
	if (cubeIsActive) { //cube is active

		Globals::cube.spin(rotationDirection);
	}
	else if (sphereIsActive){//sphere is active
		Globals::cube.applyVelocity();
	}
	else if (houseIsActive)
	{
		
	}
	displayCallback();         // call display routine to show the cube
}

//----------------------------------------------------------------------------
// Callback method called by GLUT when graphics window is resized by the user
void Window::reshapeCallback(int w, int h)
{
	cerr << "Window::reshapeCallback called" << endl;
	width = w;
	height = h;
	glViewport(0, 0, w, h);  // set new viewport size
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60.0, double(width) / (double)height, 1.0, 1000.0); // set perspective projection viewing frustum
	glTranslatef(0, 0, -20);    // move camera back 20 units so that it looks at the origin (or else it's in the origin)
	glMatrixMode(GL_MODELVIEW);
}

//----------------------------------------------------------------------------
// Callback method called by GLUT when window readraw is necessary or when glutPostRedisplay() was called.
void Window::displayCallback()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  // clear color and depth buffers
	glMatrixMode(GL_MODELVIEW);  // make sure we're in Modelview mode

	// Tell OpenGL what ModelView matrix to use:

	if (cubeIsActive || sphereIsActive)
	{
		Matrix4 glmatrix;
		glmatrix = Globals::cube.getMatrix();
		glmatrix.transpose();
		glLoadMatrixd(glmatrix.getPointer());
	}
	else if (houseIsActive)
	{
		glLoadMatrixd(Globals::camera.getGLMatrix().getPointer());
	}
	else if (bunnyIsActive || dragonIsActive)
	{
		Matrix4 glmatrix;
		Matrix4 scale;
		Matrix4 translate;
		Vector3 midPoint = Globals::xyzParser.getCenter();
		translate.makeTranslate(-midPoint.getX()+.005, -midPoint.getY()+.01, -midPoint.getZ());
		scale.makeScale(108, 108, 108);
		glmatrix = scale*translate;
		glmatrix.transpose();
		glLoadMatrixd(glmatrix.getPointer());
		if (canPrintBunny || canPrintDragon)
		{
			translate.print("Translation Matrix");
			scale.print("Scale Matrix");
		}
		canPrintBunny = false; canPrintDragon = false; //do not spam the screen.
	}


	if (sphereIsActive)
	{
		glutSolidSphere(1, 50, 50);
	}
	else if (cubeIsActive)
	{
		// Draw all six faces of the cube:
		glBegin(GL_QUADS);
		glColor3f(color[0], color[1], color[2]);		// This makes the cube green; the parameters are for red, green and blue. 
		// To change the color of the other faces you will need to repeat this call before each face is drawn.
		// Draw front face:
		glNormal3f(0.0, 0.0, 1.0);
		glVertex3f(-5.0, 5.0, 5.0);
		glVertex3f(5.0, 5.0, 5.0);
		glVertex3f(5.0, -5.0, 5.0);
		glVertex3f(-5.0, -5.0, 5.0);

		// Draw left side:
		glNormal3f(-1.0, 0.0, 0.0);
		glVertex3f(-5.0, 5.0, 5.0);
		glVertex3f(-5.0, 5.0, -5.0);
		glVertex3f(-5.0, -5.0, -5.0);
		glVertex3f(-5.0, -5.0, 5.0);

		// Draw right side:
		glNormal3f(1.0, 0.0, 0.0);
		glVertex3f(5.0, 5.0, 5.0);
		glVertex3f(5.0, 5.0, -5.0);
		glVertex3f(5.0, -5.0, -5.0);
		glVertex3f(5.0, -5.0, 5.0);

		// Draw back face:
		glNormal3f(0.0, 0.0, -1.0);
		glVertex3f(-5.0, 5.0, -5.0);
		glVertex3f(5.0, 5.0, -5.0);
		glVertex3f(5.0, -5.0, -5.0);
		glVertex3f(-5.0, -5.0, -5.0);

		// Draw top side:
		glNormal3f(0.0, 1.0, 0.0);
		glVertex3f(-5.0, 5.0, 5.0);
		glVertex3f(5.0, 5.0, 5.0);
		glVertex3f(5.0, 5.0, -5.0);
		glVertex3f(-5.0, 5.0, -5.0);

		// Draw bottom side:
		glNormal3f(0.0, -1.0, 0.0);
		glVertex3f(-5.0, -5.0, -5.0);
		glVertex3f(5.0, -5.0, -5.0);
		glVertex3f(5.0, -5.0, 5.0);
		glVertex3f(-5.0, -5.0, 5.0);
		glEnd();
	}
	else if (houseIsActive)
	{
		house.makeHouse();
	}
	else if (bunnyIsActive)
	{
		Globals::xyzParser.displayParsedFile();
	}
	else if (dragonIsActive)
	{
		Globals::xyzParser.displayParsedFile();
	}
	glFlush();
	glutSwapBuffers();
}
