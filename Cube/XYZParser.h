#ifndef _XYZParser_
#define _XYZParser_

#include <string>
#include <vector>
#include "Vector3.h"
using namespace std;

class XYZParser
{
private:
	struct line
	{
		double x = 0;
		double y = 0;
		double z = 0;
		double xNorm = 0;
		double yNorm = 0;
		double zNorm = 0;
	} XYZLine;

	std::vector<line> XYZVector;

	void resetBounds();
	void recalculateBounds();
	void normalizeStruct();
public:
	double minX;
	double minY;
	double minZ;
	double maxX;
	double maxY;
	double maxZ;

	XYZParser();
	~XYZParser();
	void parseFile(string fileName);
	void displayParsedFile();
	Vector3 XYZParser::getCenter();
	void printBounds();
};
#endif