#ifndef _CUBE_H_
#define _CUBE_H_

#include "Matrix4.h"

class Cube
{
  private:
	const double RAND_AMT = 6000.0;
	const int THRESHOLD = 10;
	const int SIDE_THRESHOLD = 10;
	const double ACCELERATION = .01;
  protected:
    Matrix4 model2world;            // model matrix (transforms model coordinates to world coordinates)
    double angle;                   // rotation angle [degrees]
	Vector3 velocity;
  public:
    Cube();   // Constructor
    Matrix4& getMatrix();
	void setMatrix(Matrix4& m);
    void spin(double deg);      // spin cube [degrees]
	void translate(double x, double y, double z);
	void reset();
	void scale(double amt);
	void orbitZ(double deg);
	void applyVelocity();
	void print();
	void randomizeVelocity();
};

#endif

