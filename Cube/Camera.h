#ifndef _CAMERA_
#define _CAMERA_
#include "Vector3.h"
#include "Matrix4.h"
#include <GL/glut.h>
class Camera
{
private:
	Vector3 e; //center of projection
	Vector3 d; //"look at point"
	Vector3 up; //up vector

	Vector3 Xc; //camera x-axis
	Vector3 Yc; //camera y-axis
	Vector3 Zc; //camera z-axis
	Matrix4 camera2world; //the inverse camera matrix
	Matrix4 world2camera; //the camera matrix ("C")
	void setCameraAxis();
public:
	Camera();
	~Camera();
	Matrix4 getWorld2Camera();
	Matrix4 getCamera2World();
	Matrix4 getGLMatrix();
	void setWorld2Camera();
	void setCamera2World();
	//setters:
	void setE(double x, double y, double z);
	void setD(double x, double y, double z);
	void setUp(double x, double y, double z);
};
#endif
