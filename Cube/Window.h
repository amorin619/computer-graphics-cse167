#ifndef _WINDOW_H_
#define _WINDOW_H_
#include "House.h"

class Window	  // OpenGL output window related routines
{
  public:
	static double color[3];
    static int width, height; 	            // window size
	static double rotationDirection;
	static bool sphereIsActive;
	static bool cubeIsActive;
	static bool houseIsActive;
	static bool bunnyIsActive;
	static bool dragonIsActive;
	static bool canPrintBunny;
	static bool canPrintDragon;
    static void idleCallback(void);
    static void reshapeCallback(int, int);
    static void displayCallback(void);
	
private:
	//XYZParser xyzParser; //same as below
	//House house; //This gave a weird error for some reason, moved to window.cpp
};

#endif

